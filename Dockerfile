# pull official base image
FROM debian

# install system dependencies
RUN apt update && apt install -y build-essential cmake gdb valgrind

# set work directory
WORKDIR /usr/src/build
