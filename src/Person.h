
#ifndef PERSON_H_
#define PERSON_H_

#include <string>

using std::string;

class Person {
 public:
  Person() = delete;
  Person(string first_name, string last_name);
  virtual ~Person();
  virtual string summary() const;
  virtual string work() const = 0;

 protected:
  string first_name_;
  string last_name_;
};

#endif // PERSON_H_
