// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "Student.h"

#include <iostream>

using std::cout;
using std::endl;

Student::Student(string first_name, string last_name, int year, char group):
    Person(first_name, last_name),
    year_(year),
    group_(group) {
//  cout << "In Student ctor" << endl;
}

Student::~Student() {
//  cout << "In Student dtor" << endl;
}

string Student::summary() const {
  return first_name_ + " " + last_name_ + " - " + std::to_string(year_) +
         "th year student (group " + group_ + ")";
}

string Student::work() const {
  return "studying";
}
