#include <cstdlib>
#include <iostream>
#include <vector>

#include "IntlStudent.h"
#include "Person.h"
#include "Student.h"
#include "Teacher.h"

using std::cout;
using std::endl;

int main() {
  std::vector<Person*> people;

  people.push_back(new Teacher("Fitz", "Farseer"));
  people.push_back(new IntlStudent("John", "Smith", 3, 'B', "UK"));
  people.push_back(new Student("Jon", "Snow", 3, 'A'));

  for (const auto& person : people) {
    cout << person->summary() << endl;
    cout << "  " << person->work() << endl;
  }

  for (auto person : people) {
    delete person;
  }

  return EXIT_SUCCESS;
}
