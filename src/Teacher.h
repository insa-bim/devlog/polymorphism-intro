// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef POLYMORPHISM_INTRO_INIT_TEACHER_H
#define POLYMORPHISM_INTRO_INIT_TEACHER_H

#include "Person.h"

#include <string>

class Teacher : public Person {
 public:
  Teacher() = delete;
  Teacher(const Teacher&) = delete;
  Teacher(Teacher&&) = delete;
  Teacher& operator=(const Teacher&) = delete;
  Teacher& operator=(Teacher&&) = delete;
  virtual ~Teacher() = default;

  Teacher(string first_name, string last_name);

  virtual string work() const override;

 protected:
};


#endif  // POLYMORPHISM_INTRO_INIT_TEACHER_H
