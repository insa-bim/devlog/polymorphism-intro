// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "IntlStudent.h"

IntlStudent::IntlStudent(string first_name, string last_name, int year, char group, string country):
    Student(first_name, last_name, year, group),
    country_(country) {
//  cout << "In IntlStudent ctor" << endl;
}

string IntlStudent::summary() const {
  return first_name_ + " " + last_name_ + " - " + std::to_string(year_) +
         "th year student (group " + group_ + " - " + country_ + ")";
}
