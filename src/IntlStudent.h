// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef POLYMORPHISM_INTRO_INIT_INTLSTUDENT_H
#define POLYMORPHISM_INTRO_INIT_INTLSTUDENT_H

#include "Student.h"

#include <string>

using std::string;

class IntlStudent : public Student {
 public:
  IntlStudent() = delete;
  IntlStudent(const IntlStudent&) = delete;
  IntlStudent(IntlStudent&&) = delete;
  IntlStudent& operator=(const IntlStudent&) = delete;
  IntlStudent& operator=(IntlStudent&&) = delete;
  virtual ~IntlStudent() = default;

  IntlStudent(string first_name, string last_name, int year, char group, string country);

  string summary() const override;

 protected:
  string country_;
};


#endif  // POLYMORPHISM_INTRO_INIT_INTLSTUDENT_H
