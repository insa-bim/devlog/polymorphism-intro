#include "Person.h"

#include <iostream>
#include <string>

using std::cout;
using std::endl;

Person::Person(string first_name, string last_name):
    first_name_(first_name),
    last_name_(last_name) {
//  cout << "In Person ctor" << endl;
}

Person::~Person() {
//  cout << "In Person dtor" << endl;
}

string Person::summary() const {
  return first_name_ + " " + last_name_;
}
